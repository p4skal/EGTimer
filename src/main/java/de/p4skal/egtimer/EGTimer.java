package de.p4skal.egtimer;


import de.p4skal.egtimer.listeners.QuitListener;
import de.p4skal.egtimer.modules.TimerModule;
import de.p4skal.egtimer.servers.GommeHDNetServer;
import lombok.Getter;
import net.labymod.api.LabyModAddon;
import net.labymod.settings.elements.SettingsElement;

import java.util.List;

/**
 * Main Entry-Point for LabyMod.
 *
 * TODO:
 *  - may add account manager compatibility
 *
 * @author p4skal
 */
@Getter
public class EGTimer extends LabyModAddon {
    /* --MODULES-- */
    private TimerModule timerModule;

    /* --SERVERS-- */
    private GommeHDNetServer gommeHDNetServer;

    /* --LISTENERS-- */
    private QuitListener quitListener;

    /**
     * Called when Addon get's enabled.
     */
    @Override
    public void onEnable( ) {
        /* --MODULES-- */

        // Register TimerModule.
        this.getApi().registerModule( this.timerModule = new TimerModule( this ) );


        /* --SERVERS-- */

        // Register GommeHDNetServer Support.
        this.getApi().registerServerSupport( this, this.gommeHDNetServer = new GommeHDNetServer( this ) );


        /* --LISTENERS-- */

        // Register Quit Listener.
        this.getApi().getEventManager().registerOnQuit( this.quitListener = new QuitListener( this ) );
    }

    /**
     * Called when Addon get's disabled.
     */
    @Override
    public void onDisable( ) {
    }

    /**
     * Called when Config get's load.
     */
    @Override
    public void loadConfig( ) {
    }

    /**
     * Called when the addon's Ingame-Settings should be filled.
     *
     * @param subSettings A list containing the addon's settings' elements.
     */
    @Override
    protected void fillSettings( List<SettingsElement> subSettings ) {
    }
}
