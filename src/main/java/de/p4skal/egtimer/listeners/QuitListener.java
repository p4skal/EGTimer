package de.p4skal.egtimer.listeners;

import de.p4skal.egtimer.EGTimer;
import lombok.AllArgsConstructor;
import lombok.Getter;
import net.labymod.utils.Consumer;
import net.labymod.utils.ServerData;

/**
 * Represent Quit Listener. Handle from Countdown reset on quit.
 *
 * @author p4skal
 */
@Getter
@AllArgsConstructor
public class QuitListener implements Consumer<ServerData> {
    /**
     * Main Class of the Addon.
     */
    private final EGTimer egTimer;

    /**
     * Called when Player leaves Server.
     *
     * @param serverData The specified Data of the Server.
     */
    @Override
    public void accept( ServerData serverData ) {
        boolean gommeServer = false;

        // Iterate Address Names.
        for ( String address : this.egTimer.getGommeHDNetServer().getAddressNames() ) {
            // Check if IP equals Address.
            if ( serverData.getIp().equalsIgnoreCase( address ) ) {
                // Set Gomme Server.
                gommeServer = true;
                break;
            }
        }

        // Check for not Gomme.
        if ( !gommeServer )
            return;

        // Set Started.
        this.egTimer.getGommeHDNetServer().setStarted( false );
    }
}
