package de.p4skal.egtimer.modules;

import de.p4skal.egtimer.EGTimer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import net.labymod.ingamegui.enums.EnumItemSlot;
import net.labymod.ingamegui.moduletypes.ItemModule;
import net.labymod.main.LabyMod;
import net.labymod.settings.elements.ControlElement;
import net.labymod.utils.Material;
import net.labymod.utils.ModColor;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

/**
 * Represent the Timer Module. Handle Design / UI of Module
 *
 * @author p4skal
 */
@Data
@RequiredArgsConstructor
@EqualsAndHashCode( callSuper = true )
public class TimerModule extends ItemModule {
    /**
     * Main Class of the Addon.
     */
    private final EGTimer egTimer;
    /**
     * The Item of the Module.
     */
    private final ItemStack enderPearl = new ItemStack( Item.getItemById( 368 ), 1 );
    /**
     * The IconData of the Module.
     */
    private final ControlElement.IconData iconData = new ControlElement.IconData( Material.ENDER_PEARL );
    /**
     * The Time of the CountDown start.
     */
    private long teleportStartTime;

    /**
     * Called when Module get's drawn.
     *
     * @param x        The X-Coordinate.
     * @param y        The Y-Coordinate.
     * @param rightX   The Coordinate when Alignment is right bounded.
     * @param itemSlot The Slot of the Item.
     */
    @Override
    public void draw( int x, int y, int rightX, EnumItemSlot itemSlot ) {
        // Call Super.
        super.draw( x, y, rightX, itemSlot );

        // Get Size.
        double width = getWidth();
        double height = getHeight();

        // Calculate Remain.
        int remain = 45 - (int) ( System.currentTimeMillis() / 1000 - teleportStartTime );
        // Get Save Status.
        boolean isSave = remain >= 0;

        // Check for save.
        if ( !isSave )
            // Add 30s Teleport Time.
            remain += 30;

        // Check if Remain is smaller than 0.
        if ( remain < 0 )
            // Adjust Remain to 0.
            remain = 0;

        // Build Text.
        String text = ModColor.cl( !isSave ? "c" : "a" ) + remain + ( isSave ? " [Save zone]" : " [Teleport zone]" );

        // Draw Text.
        LabyMod.getInstance().getDrawUtils().drawString( text, x + width + 5, y + height / 2 - 2 );
    }

    /**
     * Called when PlaceholderTexture get's loaded.
     *
     * @return The ResourceLocation of the PlaceholderTexture.
     */
    @Override
    public ResourceLocation getPlaceholderTexture( ) {
        return null;
    }

    /**
     * Called when Module get's drawn.
     *
     * @return True if Module should be visible.
     */
    @Override
    public boolean isShown( ) {
        return this.egTimer.getGommeHDNetServer().isEnderGames() && this.egTimer.getGommeHDNetServer().isStarted();
    }

    /**
     * Get Item for the ItemModule.
     *
     * @return The Item as ItemStack.
     */
    @Override
    public ItemStack getItem( ) {
        return this.enderPearl;
    }

    /**
     * Called when Module get's drawn.
     *
     * @return True, durability should be drawn.
     */
    @Override
    public boolean drawDurability( ) {
        return false;
    }

    /**
     * Called when the IconData of the Module get's loaded.
     *
     * @return The IconData.
     */
    @Override
    public ControlElement.IconData getIconData( ) {
        return this.iconData;
    }

    /**
     * Called when Settings should be loaded.
     */
    @Override
    public void loadSettings( ) {
    }

    /**
     * Get the Control Name of the Module.
     *
     * @return The Control Name as String.
     */
    @Override
    public String getControlName( ) {
        return "EG Teleport Module";
    }

    /**
     * Get the Setting Name of the Module.
     *
     * @return The Setting Name as String.
     */
    @Override
    public String getSettingName( ) {
        return "gommehdnet_eg_module";
    }

    /**
     * Get the Description of the Module.
     *
     * @return The Description as String.
     */
    @Override
    public String getDescription( ) {
        return "Show EG Timer in EnderGames on GommeHD.net.";
    }

    /**
     * Get sorting of the Module.
     *
     * @return SortingId as int.
     */
    @Override
    public int getSortingId( ) {
        return 0;
    }
}
