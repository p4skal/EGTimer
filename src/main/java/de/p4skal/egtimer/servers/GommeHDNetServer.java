package de.p4skal.egtimer.servers;

import de.p4skal.egtimer.EGTimer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.labymod.api.events.TabListEvent;
import net.labymod.servermanager.ChatDisplayAction;
import net.labymod.servermanager.Server;
import net.labymod.settings.elements.SettingsElement;
import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.ServerData;
import net.minecraft.network.PacketBuffer;

import java.util.List;
import java.util.regex.Pattern;

/**
 * Server-Support for GommeHDNetServer. Handle EnderGames / EG Timer integration.
 * Prepare Data for {@link de.p4skal.egtimer.modules.TimerModule}.
 *
 * @author p4skal
 */
@Data
@EqualsAndHashCode( callSuper = true )
public class GommeHDNetServer extends Server {
    /**
     * Main Class of the Addon.
     */
    private final EGTimer egTimer;
    /**
     * True, if current CloudType is EnderGames.
     */
    private boolean enderGames;
    /**
     * True, if EnderGames is started.
     */
    private boolean started = false;
    /**
     * Contains expression for the German Teleport Message.
     */
    private final Pattern germanTeleport = Pattern.compile( "Du wurdest mit \\w+ getauscht" );
    /**
     * Contains expression for the English Teleport Message.
     */
    private final Pattern englishTeleport = Pattern.compile( "You have been swapped with \\w+" );
    /**
     * Contains expression for the German Grace Period Message.
     */
    private final Pattern germanGracePeriod = Pattern.compile( "Die Schutzzeit ist beendet!" );
    /**
     * Contains expression for the English Grace Period Message.
     */
    private final Pattern englishGracePeriod = Pattern.compile( "The grace period is over" );
    /**
     * Contains expression for the German Win Message.
     */
    private final Pattern germanWin = Pattern.compile( "\\w+ hat die EnderGames gewonnen!" );
    /**
     * Contains expression for the English Win Message.
     */
    private final Pattern englishWin = Pattern.compile( "\\w+ has won the EnderGames" );
    /**
     * Contains expression for the German No-Win Message.
     */
    private final Pattern germanNoWin = Pattern.compile( "Niemand konnte sich als Sieger erweisen!" );
    /**
     * Contains expression for the English No-Win Message.
     */
    private final Pattern englishNoWin = Pattern.compile( "Sadly no one won the EnderGames this time" );
    /**
     * Contains expression for the German Death Message.
     */
    private Pattern germanDeath = Pattern.compile( "-" );
    /**
     * Contains expression for the English Death Message.
     */
    private Pattern englishDeath = Pattern.compile( "-" );
    /**
     * Contains expression for the German Death by Other Message.
     */
    private Pattern germanDeath2 = Pattern.compile( "-" );
    /**
     * Contains expression for the English Death by Other Message.
     */
    private Pattern englishDeath2 = Pattern.compile( "-" );

    /**
     * Constructor of GommeHDNetServer.
     *
     * @param egTimer The Main Class of the Addon.
     */
    public GommeHDNetServer( EGTimer egTimer ) {
        // Call Super.
        super( "gommehd", "gommehd.net" );
        // Pass Main Class,
        this.egTimer = egTimer;
    }

    /**
     * Called when Client joins the Server.
     * Fill Patterns with current User Name.
     *
     * @param serverData The specified Data of the Server.
     */
    @Override
    public void onJoin( ServerData serverData ) {
        // Fill German Death.
        this.germanDeath = Pattern.compile( Minecraft.getMinecraft().thePlayer.getName() + " ist gestorben!" );
        // Fill English Death.
        this.englishDeath = Pattern.compile( Minecraft.getMinecraft().thePlayer.getName() + " {2}died" );
        // Fill German Death by Other.
        this.germanDeath2 = Pattern.compile( Minecraft.getMinecraft().thePlayer.getName() + " wurde von \\w++ get" );
        // Fill English Death by Other.
        this.englishDeath2 = Pattern.compile( Minecraft.getMinecraft().thePlayer.getName() + " was slain by \\w+" );
    }

    /**
     * This Method will be called, when the Client receives a Chat Message from this Server.
     *
     * @param clean     The Message without Color-Codes.
     * @param formatted The Message with Color-Codes.
     * @return How Chat-Message should be handled.
     */
    @Override
    public ChatDisplayAction handleChatMessage( String clean, String formatted ) {
        // Check for EnderGames.
        if ( !this.enderGames )
            return null;

        // Check if Message is Teleport Message.
        if ( this.englishTeleport.matcher( clean ).find() || this.germanTeleport.matcher( clean ).find() )
            // Set new Start Time for Countdown.
            this.egTimer.getTimerModule().setTeleportStartTime( System.currentTimeMillis() / 1000 );

        // Check if Message is Grace Period-End Message.
        if ( this.germanGracePeriod.matcher( clean ).find() || this.englishGracePeriod.matcher( clean ).find() ) {
            // Set new Start Time for Countdown.
            this.egTimer.getTimerModule().setTeleportStartTime( System.currentTimeMillis() / 1000 );
            // Set EnderGames started.
            this.started = true;
        }

        // Check for Win or Death.
        if ( this.germanWin.matcher( clean ).find() || this.englishWin.matcher( clean ).find() ||
                this.germanNoWin.matcher( clean ).find() || this.germanNoWin.matcher( clean ).find() ||
                this.germanDeath.matcher( clean ).find() || this.englishDeath.matcher( clean ).find() ||
                this.germanDeath2.matcher( clean ).find() || this.englishDeath2.matcher( clean ).find() )
            // Set started.
            this.started = false;


        return null;
    }

    /**
     * This Method will be called, when the Client receives a Plugin-Message from this Server.
     *
     * @param channelName  The Name of the Channel.
     * @param packetBuffer The Payload of the Message.
     */
    @Override
    public void handlePluginMessage( String channelName, PacketBuffer packetBuffer ) {
    }

    /**
     * Called when the Tablist-Header or Footer gets updated on this Server.
     *
     * @param type            Type of Message. Header or Footer.
     * @param formattedText   The Message with Color-Codes.
     * @param unformattedText The Message without Color-Codes.
     */
    @Override
    public void handleTabInfoMessage( TabListEvent.Type type, String formattedText, String unformattedText ) {
        // Check for Header Type.
        if ( type == TabListEvent.Type.HEADER ) {
            // Check if current Tab Header contains Ender Games.
            boolean newEnderGames = formattedText.contains( "Ender" );

            // Check if Player leaves EnderGames.
            if ( this.enderGames && !newEnderGames )
                // Set Started.
                this.started = false;

            // Search for Ender Games.
            this.enderGames = newEnderGames;
        }
    }

    /**
     * Called after the Server has been added.
     *
     * @param subSettings The Addon's sub settings
     */
    @Override
    public void fillSubSettings( List<SettingsElement> subSettings ) {

    }
}
